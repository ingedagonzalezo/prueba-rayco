$('#success-alert').hide();

$('#num1').keypress(function (event) {
    return isNumber(event, this)
});

$('#num2').keypress(function (event) {
    return isNumber(event, this)
});


$("#num1").on("change paste keyup", function() {
    $('#result').val('');
    $('#result').removeClass('result')
 });

 $("#num2").on("change paste keyup", function() {
    $('#result').val('');
    $('#result').removeClass('result')
 });

 $("#cmbOperar").on("change paste keyup", function() {
    $('#result').val('');
    $('#result').removeClass('result')
 });

// THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
function isNumber(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (
        (charCode != 46 || $(element).val().indexOf('.') != -1) && // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function zero() {
    if ($('#cmbOperar').val() == '4' && $('#num2').val() == '0') {
        $('#aleryZero').show();
        $('#aleryZero').val('error')

    }
}

$('#btnClean').click(function (e) {
    e.preventDefault();

    $('#num1').val('');
    $('#num2').val('');
    $('#cmbOperar').val('');
    $('#result').val('');
    $('#result').removeClass('result')

    $('#num1').focus();
});

$('#btnCal').click(function (e) {
    e.preventDefault();

    if ($('#num1').val() == '' && $('#num2').val() == '') {

        $('#success-alert').show();
        $('#success-alert').html('Para realizar una operación, debe digitar dos numeros.');
        window.setTimeout(function () {
            $("#success-alert").hide();
        }, 5000);

        $('#num1').focus();
        return;
    } else if ($('#num1').val() == '') {

        $('#success-alert').show();
        $('#success-alert').html('La casilla del primer número esta vacia.');
        window.setTimeout(function () {
            $("#success-alert").hide();
        }, 5000);

        $('#num1').focus();
        return;

    } else if ($('#num2').val() == '') {

        $('#success-alert').show();
        $('#success-alert').html('La casilla del segundo número esta vacia.');
        window.setTimeout(function () {
            $("#success-alert").hide();
        }, 5000);

        $('#num2').focus();
        return;

    } else if ($('#cmbOperar').val() == '') {

        $('#success-alert').show();
        $('#success-alert').html('Debe seleccionar una operación.');
        window.setTimeout(function () {
            $("#success-alert").hide();
        }, 5000);

        $('#cmbOperar').focus();
        return;

    } else if ($('#cmbOperar').val() == '4' && $('#num2').val() == '0') {

        $('#success-alert').show();
        $('#success-alert').html('No se puede dividir por 0.');
        window.setTimeout(function () {
            $("#success-alert").hide();
        }, 5000);

        $('#num2').focus();
        $('#num2').val('');
        return;

    }
    var data = {
        num1: $('#num1').val(),
        num2: $('#num2').val(),
        opt: $('#cmbOperar').val()
    };


    var url = "Controlador.php";
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        beforeSend: function () {},
        success: function (data) {
            console.log(data);
            $('#result').val(data);
            $('#result').addClass('result')
        }

    });

});